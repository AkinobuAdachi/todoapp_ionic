import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';


@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.page.html',
  styleUrls: ['./add-item.page.scss'],
})
export class AddItemPage implements OnInit {

  title="";
  description="";
  constructor(public modalCtrl: ModalController, public navCtrl: NavController) { }

  ngOnInit() {
  }

  save(){
    this.modalCtrl.dismiss({title:this.title, description:this.description});
    // this.navCtrl.back();
    // console.log(event);
  }

  cancel(){
    this.modalCtrl.dismiss();
  }
}
