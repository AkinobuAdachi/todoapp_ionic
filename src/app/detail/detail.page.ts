import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  // title: string;
  // description: string;
  item: Object;
  constructor(public navParams: NavParams, public modalCtrl: ModalController) {
    console.log(this.navParams.get("args"));
    this.item = this.navParams.get("args");
    // this.title = this.navParams.get("args").title;
    // this.description = this.navParams.get("args").description;
   }

  ngOnInit() {
  }

  close(){
    this.modalCtrl.dismiss({isDelete: false, item: null});
  }

  delete(){
    this.modalCtrl.dismiss({isDelete: true, item: this.item});
  }
}
