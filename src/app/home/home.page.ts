import { Component } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular'
import { AddItemPage } from '../add-item/add-item.page';
import { DetailPage } from '../detail/detail.page';

import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  data: string = 'Hello';
  list = [];
  constructor(
    public modalCtrl: ModalController, 
    public navCtrl: NavController,
    public storage: Storage
    ){
    this.loadStorage()
    this.addItem({title:"task1", description:"desc1"});
    this.addItem({title:"task2", description:"desc2"});
  }

  loadStorage(){
    this.storage.get("data")
      .then((val) => {
        this.list = val;
      });
  }

  async showAddTask() {
    const modal = await this.modalCtrl.create({component: AddItemPage});
    this.addPageOnDismiss(modal);
    modal.present();
  }

  async addPageOnDismiss(modal){
    let newTask = (await modal.onDidDismiss()).data;
    if(newTask != null && newTask.title != ""){
      this.addItem(newTask);
    }
  }

  async showDetail(item) {
    let modal = await this.modalCtrl.create({component: DetailPage, componentProps: {args:item}});
    this.detailPageOnDismiss(modal);
    modal.present();
    // this.navCtrl.navigateForward('/detail');
  }
  
  async detailPageOnDismiss(modal){
    let args = (await modal.onDidDismiss()).data;
    // console.log(args);
    if(args.isDelete == true) {
      this.deleteItem(args);
    }
  }

  addItem(newTask){
    this.list.push(newTask);
    this.storage.set("data", this.list);
  }

  deleteItem(args) {
    this.list = this.list.filter( n => n != args.item);
    this.storage.set("data", this.list);
  }
}
